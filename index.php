<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <title>ilab</title>
</head>
<body>
    <div class="main-outer">
      <div class="header">
        <div class="link fl_l">О Компаний<span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span></div>
        <div class="link fl_l">Новости</div>
        <div class="link fl_l">Акций</div>
        <div class="link fl_l">Вакансий</div>
        <div class="link fl_l">Контакты</div>
        <div class="link fl_l">Еще<span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span></div>
        <div class="link fl_r"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>Личный кабинет</div>
        <div class="link fl_r"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>Написать нам</div>
      </div>
      <div class="company">
        <div class="logo fl_l">
          <img src="images/company-logo.png" />
        </div>
        <div class="phones fl_l">
          <span class="glyphicon glyphicon glyphicon-earphone" ></span>(888) <b>88888</b><br/>
          <span class="glyphicon glyphicon-phone" ></span>(888) <b>88888</b>
        </div>
        <div class="actions fl_l">
          <div class="s-3 fl_l">
            <span class="glyphicon glyphicon-road"></span>Доставка
          </div>
          <div class="s-3 fl_l">
            <span class="glyphicon glyphicon-euro"></span>Оплата
          </div>
          <div class="s-3 fl_l">
            <span class="glyphicon glyphicon-shopping-cart"></span>Корзина
          </div>
        </div>
      </div>
      <div class="clearfix" />
      <div style="color: #999; padding: 3px;">
      Works!!! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ligula justo, iaculis in porta ac, gravida in nisl. Proin et mi non eros ultricies tempor. Mauris efficitur elit ut efficitur ultricies. Quisque id feugiat magna. Sed nisl metus, suscipit vitae aliquet vel, consequat ut sapien. Mauris porta suscipit urna ut blandit. Pellentesque interdum iaculis mi, ac semper dui dapibus nec. In a nisi nec leo pulvinar sodales.

Duis et ultrices metus. Suspendisse ac lorem at risus mattis semper venenatis euismod tortor. Phasellus fringilla lacinia tincidunt. Praesent semper sed ligula sed egestas. In at commodo quam. Morbi malesuada pulvinar nisi vel gravida. Proin commodo dictum dui vitae elementum. Fusce aliquam ullamcorper lectus, sed posuere lorem placerat id. Nulla mauris erat, convallis ac orci sed, vestibulum hendrerit ligula. Vestibulum porttitor sem at faucibus faucibus. Donec luctus ante lorem, cursus euismod ex auctor ut. Phasellus egestas dolor fermentum, laoreet eros sed, suscipit ante. Morbi quis eros semper tortor molestie ultricies. Quisque turpis dui, eleifend at lorem in, lacinia dictum purus. Sed rhoncus odio ut ex porta, in fringilla mauris efficitur.
      </div>
    </div>
</body>
</html>
